import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Worker } from '../worker';
import { StaffService } from '../staff.service';

@Component({
  selector: 'app-staff-detail',
  templateUrl: './staff-detail.component.html',
  styleUrls: [ './staff-detail.component.css' ]
})
export class StaffDetailComponent implements OnInit {
  worker: Worker;

  constructor(
    private route: ActivatedRoute,
    private staffService: StaffService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getWorker();
  }

  getWorker(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.staffService.getWorker(id)
      .subscribe(worker => this.worker = worker);
  }

  goBack(): void {
    this.location.back();
  }
}
