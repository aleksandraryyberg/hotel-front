import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Booking} from './booking';
import {OperationResultBooking} from './operation-result-booking';
import {catchError} from 'rxjs/operators';
import { HttpErrorHandler } from './http-error-handler-service.service';
import {HandleError} from './http-error-handler-service.service';


// class be injected to the component
@Injectable({
  providedIn: 'root'
})

export class BookingService {
  private baseUrl = 'http://localhost:8080/api/booking';
  private httpp;
  private handleError: HandleError;
  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.httpp = http;
    this.handleError = httpErrorHandler.createHandleError('BookingService');
  }
  getBooking(): Observable<Booking[]> {
    return this.httpp.get(`${this.baseUrl}/allBookings`);
  }
  // tslint:disable-next-line:ban-types
  addBooking(booking: Booking): Observable<OperationResultBooking> {
    console.log('booking NR control' + JSON.stringify(booking));
    if (typeof(booking.bookingNr) === undefined) {
      return this.httpp.put(`${this.baseUrl}`, booking).
        pipe(
          catchError(this.handleError('addBooking', 'Error making reservation'))
      );
    } else {
      return this.httpp.post(`${this.baseUrl}`, booking).
      pipe(
        catchError(this.handleError('addBooking', null))
      );
    }
  }



  // tslint:disable-next-line:ban-types
  updateBooking(bookingNr: number, value: any): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${bookingNr}`, value);
  }

  deleteBooking(bookingNr: number): Observable<any>{
    return this.http.delete(`${this.baseUrl}/${bookingNr}`, {responseType: 'text'});
  }
}
