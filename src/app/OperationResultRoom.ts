export interface OperationResultRoom {
  isSuccess: boolean;
  message: string;
  roomId: number;
}
