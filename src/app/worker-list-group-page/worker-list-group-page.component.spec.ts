import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerListGroupPageComponent } from './worker-list-group-page.component';

describe('WorkerListGroupPageComponent', () => {
  let component: WorkerListGroupPageComponent;
  let fixture: ComponentFixture<WorkerListGroupPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkerListGroupPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerListGroupPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
