import { Component } from '@angular/core';
import { STAFF } from '../mock-staff';

@Component({
  selector: 'app-worker-list-groups-page',
  template: `
    <section>
      <h2>Worker List Group</h2>

      <app-worker-list-groups [staff]="staff" (remove)="onRemove($event)"></app-worker-list-groups>
    </section>
  `
})
export class WorkerListGroupPageComponent {
  staff = STAFF.slice();

  onRemove(id: number): void {
    this.staff = this.staff.filter(worker => worker.id !== id);
  }
}
