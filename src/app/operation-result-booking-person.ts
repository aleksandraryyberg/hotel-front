export interface OperationResultBookingPerson {
  isSuccess: boolean;
  message: string;
  id: number;
}
