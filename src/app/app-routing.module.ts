import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {RoomDetailComponent} from './room-detail/room-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {RoomComponent} from './room/room.component';
import {PricelistComponent} from './pricelist/pricelist.component';
import {StaffDetailComponent} from './staff-detail/staff-detail.component';
import {RouteGuardService} from './route-guard.service';
import {LogoutComponent} from './logout/logout.component';
import {WelcomeComponent} from './welcome/welcome.component';
import {LoginComponent} from './login/login.component';
import {ErrorComponent} from './error/error.component';
import {CrisisListComponent} from './crisis-list/crisis-list.component';
import {StaffListComponent} from './staff-list/staff-list.component';



const routes: Routes = [
  { path: 'room', component: RoomComponent },
  { path: 'room-detail', component: RoomDetailComponent },
  { path: 'room/:id', component: RoomDetailComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'pricelist', component: PricelistComponent },
  { path: 'crisis-list', component: CrisisListComponent },
  { path: 'staff-list', component: StaffListComponent  },
  { path: 'room/:id', component: DashboardComponent },
  { path: 'detail/:id', component: StaffDetailComponent },
  { path: 'staff-detail', component: StaffDetailComponent },
  { path: 'login', component: LoginComponent },
  { path: 'welcome/:name', component: WelcomeComponent, canActivate: [RouteGuardService] },
  { path: 'logout', component: LogoutComponent, canActivate: [RouteGuardService] },
  { path: '**', component: ErrorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
