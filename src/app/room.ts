export class Room {
  id: number;
  size: number;
  description: string;
  // tslint:disable-next-line:variable-name
  price: number;
}
