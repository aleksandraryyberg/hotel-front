import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerListEnterLeaveComponent } from './worker-list-enter-leave.component';

describe('WorkerListEnterLeaveComponent', () => {
  let component: WorkerListEnterLeaveComponent;
  let fixture: ComponentFixture<WorkerListEnterLeaveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkerListEnterLeaveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerListEnterLeaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
