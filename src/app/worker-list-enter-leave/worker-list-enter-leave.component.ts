import {
  Component,
  Input,
  Output,
  EventEmitter, NgModule
} from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import { STAFF } from '../mock-staff';

@Component({
  selector: 'app-worker-list-enter-leave',
  template: `
    <ul class="staff">
      <li *ngFor="let worker of staff"
          [@flyInOut]="'in'" (click)="removeWorker(worker.id)">
          <div class="inner">
            <span class="badge">{{ worker.id }}</span>
            <span>{{ worker.name }}</span>
          </div>
      </li>
    </ul>
  `,
  styleUrls: ['worker-list-enter-leave.component.css'],
  animations: [
    trigger('flyInOut', [
      state('in', style({ transform: 'translateX(0)' })),
      transition('void => *', [
        style({ transform: 'translateX(-100%)' }),
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
},
  )
export class WorkerListEnterLeaveComponent {
  @Input() staff: Worker[];

  @Output() remove = new EventEmitter<number>();

  removeWorker(id: number): void {
    this.remove.emit(id);
  }
}
