import {Component, HostBinding, OnInit} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import { slideInAnimation } from '../animations';
import {WorkerListAutoComponent} from '../worker-list-auto/worker-list-auto.component';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import {NgModule} from '@angular/core';
import {STAFF} from '../mock-staff';


@Component({
  selector: 'app-crisis-list',
  templateUrl: './crisis-list.component.html',
  styleUrls: ['./crisis-list.component.css'],
  animations: [
    slideInAnimation
  ],
})
export class CrisisListComponent {
  @HostBinding('@.disabled')
  public animationsDisabled = false;

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

  toggleAnimations(): void {
    this.animationsDisabled = !this.animationsDisabled;
  }
}

