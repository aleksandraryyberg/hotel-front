import {Component, OnInit} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs';
import {Person} from '../person';
import {Route, Router} from '@angular/router';
import {OperationResult} from '../OperationResult';
import { PersonModalComponentService } from '../person-modal/person-modal.component.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DeletePersonModalComponentService} from '../delete-person-modal/delete-person-modal.component.servise';
import {PersonService} from '../person.service';
import {FormGroup} from '@angular/forms';


@Component({
  selector: 'app-delete-person-modal',
  templateUrl: './delete-person-modal.component.html',
  styleUrls: ['./delete-person-modal.component.css']
})

export class DeletePersonModalComponent implements OnInit {
  message: string;
  registerForm: FormGroup;
  submitted = false;
  person: Person = new Person();
  posts: any;
  persons: Person[];

  constructor(
    private activeModal: NgbActiveModal,
    private personService: PersonService,
    private router: Router,
    private deletePersonModalService: DeletePersonModalComponentService
  ) { }

  ngOnInit(): void {

  }


  DeleteThisPerson(): void {
    this.submitted = true;
// stop here if form is invalid
    this.personService.deletePerson(this.person.nr).subscribe(
      response => {
        console.log(response);
        this.message = `Delete of person nr ${this.person.nr} is Successful!`;
        this.personService.getPersons().subscribe();
      }
    );
    this.personService.getPersons().subscribe(
      responce => {
        console.log(responce);
        this.persons = responce;
      }
    );
  }
  isError(): boolean{
    return false; // this.registerForm.invalid;
  }
  close(): void {
    this.activeModal.dismiss();
  }
}

