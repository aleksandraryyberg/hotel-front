import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {DeletePersonModalComponent} from './delete-person-modal.component';
import {Person} from '../person';

@Injectable({
  providedIn: 'root'
})
export class DeletePersonModalComponentService {
  private isOpen = false;


  constructor(private modalService: NgbModal) {}

  openModal(person: Person): NgbModalRef {
    if (this.isOpen) {
      return;
    }
    this.isOpen = true;
    const modalRef = this.modalService.open(DeletePersonModalComponent, {
      backdrop: 'static',
      size: 'sm'
    });
    modalRef.componentInstance.person = person;
    modalRef.result.then(
      result => {
        this.isOpen = false;
      },
      reason => {
        this.isOpen = false;
      }
    );
    return modalRef;
  }
}
