import { Component, OnInit } from '@angular/core';
import {PersonService} from '../person.service';
import {Router} from '@angular/router';
import {Person} from '../person';
import { PersonModalComponentService } from '../person-modal/person-modal.component.service';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DeletePersonModalComponentService} from '../delete-person-modal/delete-person-modal.component.servise';


@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})

export class PersonComponent implements OnInit {
  person: Person;
  persons: Person[];
  message: string;
  personNr: string;

  constructor(
    private personService: PersonService,
    private router: Router,
    private personModalService: PersonModalComponentService,
    private deletePersonModalService: DeletePersonModalComponentService
  ){ }

  checks = false;

  ngOnInit(): void {
    this.personService.getPersons().subscribe((data: Person[]) => {
      this.persons = data;
    });
  }

refreshPerson(): void {
  this.personService.getPersons().subscribe(
    responce => {
      console.log(responce);
      this.persons = responce;
    }
  );
}

deleteThisPerson(nr): void {
    this.personService.deletePerson(nr).subscribe(
      response => {
        console.log(response);
        this.message = `Delete of person nr ${nr} is Successful!`;
        this.refreshPerson();
      }
    );
  }

updateThisPerson(nr): void {
  console.log(`update ${nr}`);
  this.router.navigate([`persons`, nr]);
}

addPersonDeleteModal(person: Person): NgbModalRef {
    const modalRef = this.deletePersonModalService.openModal(person);
    return modalRef;
  }

addPersonModal(person: Person): NgbModalRef {
  const modalRef = this.personModalService.openModal(person);
  return modalRef;
  }
bulk(e): void{
    if (e.target.checked === true) {
      this.checks = true;
    } else {
      this.checks = false;
    }
  }
  selectPerson(person): void {
    this.personNr = person;
  }
  }



