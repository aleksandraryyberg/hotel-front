import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs';
import {Booking} from '../booking';
import {BookingService} from '../booking.service';
import {Person} from '../person';


@Component({
  selector: 'app-booking-modal',
  templateUrl: './booking-modal.component.html',
  styleUrls: ['./booking-modal.component.css']
})
export class BookingModalComponent implements OnInit {
  booking: any;
  person: Person;
  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit(): void {}

  close(): void {
    this.activeModal.dismiss();
  }
}
