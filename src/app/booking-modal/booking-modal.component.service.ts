import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BookingModalComponent } from './booking-modal.component';
import {Booking} from '../booking';


@Injectable({
  providedIn: 'root'
})
export class BookingModalComponentService {
  private isOpen = false;

  constructor(private modalService: NgbModal) {}

  openModal(booking: Booking): NgbModalRef {
    if (this.isOpen) {
      return;
    }
    this.isOpen = true;
    const modalRef = this.modalService.open(BookingModalComponent, {
      backdrop: 'static',
      size: 'sm'
    });
    modalRef.componentInstance.booking = booking;
    modalRef.result.then(
      result => {
        this.isOpen = false;
      },
      reason => {
        this.isOpen = false;
      }
    );
    return modalRef;
  }
}
