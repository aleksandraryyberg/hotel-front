import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Booking} from './booking';
import {OperationResultBooking} from './operation-result-booking';
import {catchError} from 'rxjs/operators';
import { HttpErrorHandler } from './http-error-handler-service.service';
import {HandleError} from './http-error-handler-service.service';
import {BookingPerson} from './booking-person';
import {OperationResultBookingPerson} from './operation-result-booking-person';

@Injectable({
  providedIn: 'root'
})

export class BookingPersonService {
  private baseUrl = 'http://localhost:8080/api/bookingperson';
  private httpp;
  private handleError: HandleError;
  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.httpp = http;
    this.handleError = httpErrorHandler.createHandleError('BookingPersonService');
  }
  getBookingPerson(): Observable<BookingPerson[]> {
    return this.httpp.get(`${this.baseUrl}/allBookingPerson`);
  }
  // tslint:disable-next-line:ban-types
  addBookingPerson(bookingPerson: BookingPerson): Observable<OperationResultBookingPerson> {
    console.log('booking NR control' + JSON.stringify(bookingPerson));
    if (typeof(bookingPerson.id) === undefined) {
      return this.httpp.put(`${this.baseUrl}`, bookingPerson).
      pipe(
        catchError(this.handleError('addBookingPerson', 'Error making reservation'))
      );
    } else {
      return this.httpp.post(`${this.baseUrl}`, bookingPerson).
      pipe(
        catchError(this.handleError('addBookingPerson', null))
      );
    }
  }
  updateBookingPerson(id: number, value: any): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`, value);
  }

  deleteBookingPerson(id: number): Observable<any>{
    return this.http.delete(`${this.baseUrl}/${id}`, {responseType: 'text'});
  }
}

