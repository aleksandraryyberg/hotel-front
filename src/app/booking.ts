import {Person} from './person';
import {Room} from './room';

export class Booking {
  bookingNr: number;
  startDate?: Date;
  endDate?: Date;
  personsC: number;
  room: Room[];
  persons: Person[];
  constructor() {
  }
}

