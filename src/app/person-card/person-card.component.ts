import { Component, OnInit, Input } from '@angular/core';
import {Observable} from 'rxjs';
import {Person} from '../person';
import {PersonService} from '../person.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HttpClient, HttpParams } from '@angular/common/http';
import {Worker} from '../worker';
import {OperationResultBooking} from '../operation-result-booking';
import {OperationResult} from '../OperationResult';

@Component({
  selector: 'app-person-card',
  templateUrl: './person-card.component.html',
  styleUrls: ['./person-card.component.css']
})
export class PersonCardComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  person: Person = new Person();
  response: OperationResult;
  @Input() personCard: Person;
  constructor(
    private personService: PersonService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private http: HttpClient) { }

  model = new Person();

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      nr: [''],
      name: ['', Validators.required],
      surname: ['', Validators.required],
      personalCode: ['', Validators.required]
    });

    if (this.personCard !== undefined && this.personCard !== null) {
      this.registerForm.controls.nr.setValue(this.personCard.nr);
      this.registerForm.controls.name.setValue(this.personCard.name);
      this.registerForm.controls.surname.setValue(this.personCard.surname);
      this.registerForm.controls.personalCode.setValue(this.personCard.personalCode);
    }
  }
  get f(): { [p: string]: AbstractControl }{
    return this.registerForm.controls; }

  onSubmit(): void{
    this.submitted = true;
// stop here if form is invalid

// Initialize Params Object
    let Params = new HttpParams();
    Params = Params.append('nullParameter', this.registerForm.value.nr);
    Params = Params.append('firstParameter', this.registerForm.value.name);
    Params = Params.append('secondParameter', this.registerForm.value.surname);
    Params = Params.append('thirdParameter', this.registerForm.value.personalCode);
    this.person.nr = this.registerForm.value.nr;
    this.person.name = this.registerForm.value.name;
    this.person.surname = this.registerForm.value.surname;
    this.person.personalCode = this.registerForm.value.personalCode;
    console.log(' onSubmit:' + JSON.stringify(this.person));
    this.personService.addPerson(this.person).subscribe(
      data => {
        console.log(' === response' + JSON.stringify(data));
        this.response = data;
      });
  }
  isError(): boolean{
    return false; // this.registerForm.invalid;
  }
  // TODO: Remove this when we're done
  get diagnostic(): string {return JSON.stringify(this.response);
  }
  /*addNewPerson(): void {
  this.personService.addPerson(this.person).subscribe(
  data => {
  console.log(data);
  });
  }*/
}
