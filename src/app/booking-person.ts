import {Person} from './person';
import {Room} from './room';
import {Booking} from './booking';

export class BookingPerson {
  id: number;
  bookingId: number;
  personId: number;
  constructor() {
  }
}
