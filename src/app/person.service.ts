import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Person} from './person';
import {OperationResult} from './OperationResult';
import { HttpErrorHandler } from './http-error-handler-service.service';
import {HandleError} from './http-error-handler-service.service';
import {catchError} from 'rxjs/operators';

// class be injected to the component
@Injectable({
  providedIn: 'root'
})

export class PersonService {
  private baseUrl = 'http://localhost:8080/api/person';
  private httpp;
  private handleError: HandleError;

  constructor(private http: HttpClient,  httpErrorHandler: HttpErrorHandler) {
    this.httpp = http;
    this.handleError = httpErrorHandler.createHandleError('PersonService');
  }

  getPersons(): Observable<Person[]> {
    return this.httpp.get(`${this.baseUrl}/all`);
  }
  addPerson(person: Person): Observable<OperationResult> {
    console.log('person NR control' + JSON.stringify(person));
    if (typeof(person.nr) === undefined || !person.nr) {
      return this.httpp.post(`${this.baseUrl}`, person).
      pipe(
        catchError(this.handleError('addPerson', 'Error adding person!'))
      );
    } else {
      return this.httpp.put(`${this.baseUrl}`, person).
      pipe(
        catchError(this.handleError('addBooking', 'Error adding person!'))
      );
    }
  }
  updatePerson(nr: number, value: any): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${nr}`, value);
  }

  deletePerson(nr: number): Observable<any>{
    return this.http.delete(`${this.baseUrl}/${nr}`, {responseType: 'text'});
  }
}
