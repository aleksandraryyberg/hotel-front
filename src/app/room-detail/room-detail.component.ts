import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {BookingService} from '../booking.service';
import {BookingModalComponentService} from '../booking-modal/booking-modal.component.service';
import { Room} from '../room';
import { RoomService } from '../room.service';
import {Booking} from '../booking';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'app-room-detail',
  templateUrl: './room-detail.component.html',
  styleUrls: [ './room-detail.component.css' ]
})
export class RoomDetailComponent implements OnInit {
  rooms: Room[];
  booking: Booking = new Booking();

  constructor(
    private route: ActivatedRoute,
    private roomService: RoomService,
    private location: Location,
    private bookingModalService: BookingModalComponentService
  ) {}

  ngOnInit(): void {
    this.roomService.getRooms().subscribe((data: Room[]) => {
      this.rooms = data;
    });
  }

  getRooms(): void {
    this.roomService.getRooms()
      .subscribe(room => this.rooms = room.slice(1, 5));
  }

  addBookingModal(booking: Booking): NgbModalRef {
    const modalRef = this.bookingModalService.openModal(booking);
    return modalRef;
  }

  goBack(): void {
    this.location.back();
  }
}
