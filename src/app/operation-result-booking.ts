export interface OperationResultBooking  {
  isSuccess: boolean;
  message: string;
  bookingNr: number;
}
