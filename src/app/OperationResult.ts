export interface OperationResult {
  isSuccess: boolean;
  message: string;
  personNr: number;
}
