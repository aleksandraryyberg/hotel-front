import {Component, Input, OnInit} from '@angular/core';
import {Booking} from '../booking';
import {Room} from '../room';
import {RoomService} from '../room.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbDate, NgbDatepickerConfig} from '@ng-bootstrap/ng-bootstrap';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BookingService} from '../booking.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Person} from '../person';
import {PersonService} from '../person.service';
import {getErrorMessage} from 'codelyzer/templateAccessibilityElementsContentRule';
import {OperationResultBooking} from '../operation-result-booking';

@Component({
  selector: 'app-booking-room',
  templateUrl: './booking-room.component.html',
  styleUrls: ['./booking-room.component.css']
})
export class BookingRoomComponent implements OnInit {
  selectedPersons: Person[] = [];
  persons: Person[];
  rooms: Room[];
  message: string;
  minDate: NgbDate;
  maxDate: NgbDate;
  form: FormGroup;
  submitted = false;
  booking: Booking = new Booking();
  checks = false;
  response: OperationResultBooking;
  @Input() bookingRoom: Booking;
  constructor( private roomService: RoomService,
               private personService: PersonService,
               private config: NgbDatepickerConfig,
               private bookingService: BookingService,
               private route: ActivatedRoute,
               private router: Router,
               private formBuilder: FormBuilder,
               private http: HttpClient) { }

  ngOnInit(): void {
    const currentDate = new Date();

    this.maxDate = new NgbDate(2100, 1, 1);
    this.config.minDate = {
      year: currentDate.getFullYear(),
      month: currentDate.getMonth() + 1,
      day: currentDate.getDate()
    };
    this.config.outsideDays = 'hidden';
    this.personService.getPersons().subscribe((data: Person[]) => {
      this.persons = data;
    });
    this.roomService.getRooms().subscribe((data: Room[]) => {
      this.rooms = data;
    });
    this.form = this.formBuilder.group({
      bookingNr: [''],
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      personsC: ['', Validators.required],
      room: ['', Validators.required],
      persons: ['', Validators.required],
    });

    if (this.bookingRoom !== undefined && this.bookingRoom !== null) {
      this.form.controls.bookingNr.setValue(this.bookingRoom.bookingNr);
      this.form.controls.startDate.setValue(this.bookingRoom.startDate);
      this.form.controls.endDate.setValue(this.bookingRoom.endDate);
      this.form.controls.personsC.setValue(this.bookingRoom.personsC);
      this.form.controls.room.setValue(this.bookingRoom.room);
      this.form.controls.persons.setValue(this.bookingRoom.persons);
    }
  }
  get f(): { [p: string]: AbstractControl }{
    return this.form.controls; }

  onSubmit(): void {
    this.submitted = true;
    let Params = new HttpParams();
    Params = Params.append('firstParameter', this.form.value.startDate);
    Params = Params.append('secondParameter', this.form.value.endDate);
    Params = Params.append('thirdParameter', this.form.value.personsC);
    Params = Params.append('fourthParameter', this.form.value.room);
    Params = Params.append('fifthParameter', this.form.value.persons);
    this.booking.bookingNr = this.form.value.bookingNr;
    this.minDate = this.form.value.startDate;
    this.booking.startDate = new Date(this.minDate.year, this.minDate.month - 1, this.minDate.day);
    this.maxDate = this.form.value.endDate;
    this.booking.endDate = new Date(this.maxDate.year, this.maxDate.month - 1, this.maxDate.day);
    this.booking.personsC = this.form.value.personsC;
    this.booking.room = this.form.value.room;
    this.booking.persons = this.selectedPersons;
    console.log(' onSubmit:' + JSON.stringify(this.booking));
    this.bookingService.addBooking(this.booking).subscribe(
      data => {
        console.log(' === response 2' + JSON.stringify(data));
        this.response = data;
      });
  }
  isError(): boolean {
    return false; // this.registerForm.invalid;
  }
  updateChecked(person, event): void {
    if (event.target.checked) {
      this.selectedPersons.push(person);
    } else {
      const i = this.selectedPersons.indexOf(person);
      this.selectedPersons.splice(i, 1);
    }
  }
  get diagnostic(): string {return JSON.stringify(this.response);
  }
}
