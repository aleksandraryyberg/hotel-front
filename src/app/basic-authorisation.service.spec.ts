import { TestBed } from '@angular/core/testing';

import { BasicAuthorisationService } from './basic-authorisation.service';

describe('BasicAuthorisationService', () => {
  let service: BasicAuthorisationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BasicAuthorisationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
