import { Component, OnInit } from '@angular/core';
import { Worker } from '../worker';
import { StaffService } from '../staff.service';

@Component({
  selector: 'app-staff-list',
  templateUrl: './staff-list.component.html',
  styleUrls: ['./staff-list.component.css']
})
export class StaffListComponent implements OnInit {
  staff: Worker[] = [];
  constructor(private staffService: StaffService) { }

  ngOnInit(): void {
    this.getStaff();
  }

  getStaff(): void {
    this.staffService.getStaff()
      .subscribe(staff => this.staff = staff.slice(1, 5));
  }
}
