export class Person {
  nr: number;
  name: string;
  surname: string;
  personalCode: number;
}
