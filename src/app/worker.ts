export class Worker {
  constructor(
  public id: number,
  public name: string,
  public lastname: string,
  public position: string,
  public age: number,
  public skill: string,
  public alterEgo?: string
) {}
}

