import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerListAutoComponent } from './worker-list-auto.component';

describe('WorkerListAutoComponent', () => {
  let component: WorkerListAutoComponent;
  let fixture: ComponentFixture<WorkerListAutoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkerListAutoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerListAutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
