import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import { Worker } from '../worker';

@Component({
  selector: 'app-worker-list-auto',
  templateUrl: 'worker-list-auto.component.html',
  styleUrls: ['worker-list-auto.component.css'],
  animations: [
    trigger('shrinkOut', [
      state('in', style({ height: '*' })),
      transition('* => void', [
        style({ height: '*' }),
        animate(250, style({ height: 0 }))
      ])
    ])
  ]
})
export class WorkerListAutoComponent {
  @Input() staff: Worker[];

  @Output() remove = new EventEmitter<number>();

  removeWorker(id: number): void {
    this.remove.emit(id);
  }
}
