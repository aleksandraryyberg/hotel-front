import { Component } from '@angular/core';
import { STAFF } from '../mock-staff';

@Component({
  selector: 'app-worker-list-enter-leave-page',
  template: `
    <section>
      <h2>Enter/Leave</h2>

      <app-worker-list-enter-leave [staff]="staff" (remove)="onRemove($event)"></app-worker-list-enter-leave>
    </section>
  `
})
export class WorkerListEnterLeavePageComponent {
  staff = STAFF.slice();

  onRemove(id: number): void {
    this.staff = this.staff.filter(worker => worker.id !== id);
  }
}
