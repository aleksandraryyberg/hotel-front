import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerListEnterLeavePageComponent } from './worker-list-enter-leave-page.component';

describe('WorkerListEnterLeavePageComponent', () => {
  let component: WorkerListEnterLeavePageComponent;
  let fixture: ComponentFixture<WorkerListEnterLeavePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkerListEnterLeavePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerListEnterLeavePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
