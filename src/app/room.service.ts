import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Room} from './room';
import {OperationResultRoom} from './OperationResultRoom';
import { HttpHeaders } from '@angular/common/http';

import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from './message.service';


// class be injected to the component
@Injectable({
  providedIn: 'root'
})

export class RoomService {
  selectedRoom: Room;
  rooms: Room[];
  private baseUrl = 'http://localhost:8080/api/room';
  private httpp;

  constructor(
    private http: HttpClient,
    private messageService: MessageService) {
    this.httpp = http;
  }

  getRooms(): Observable<Room[]> {
    return this.http.get<Room[]>(`${this.baseUrl}/allRooms`);
  }


  getRoomByNo4<Data>(id: number): Observable<Room> {
    const url = `${this.baseUrl}/?id=${id}`;
    return this.http.get<Room[]>(url)
      .pipe(
        map(rooms => rooms[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} room id=${id}`);
        }),
      );
  }

  searchRooms(term: string): Observable<Room[]> {
    if (!term.trim()) {
      // if not search term, return empty room array.
      return of([]);
    }
    return this.http.get<Room[]>(`${this.baseUrl}/?description=${term}`).pipe(
      tap(x => x.length ?
        this.log(`found rooms matching "${term}"`) :
        this.log(`no rooms matching "${term}"`)),
    );
  }

  updateRoom(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }
  deleteRoom(id: number): Observable<any>{
    return this.http.delete(`${this.baseUrl}/${id}`, {responseType: 'text'});
  }
//

  /** Log a RoomService message with the MessageService */
  private log(message: string): void {
    this.messageService.add(`RoomService: ${message}`);
  }
  onSelect(room: Room): void {
    this.selectedRoom = room;
    this.messageService.add(`RoomsComponent: Selected room id=${room.id}`);
  }
}
