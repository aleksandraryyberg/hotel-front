import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { PersonCardComponent } from './person-card/person-card.component';
import { PersonComponent } from './person/person.component';
import { WelcomeComponent } from './welcome/welcome.component';
import {FormsModule} from '@angular/forms';
import { BookingRoomComponent } from './booking-room/booking-room.component';
import { RouterModule } from '@angular/router';
import { PersonModalComponent } from './person-modal/person-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import { DeletePersonModalComponent } from './delete-person-modal/delete-person-modal.component';
import {NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import { BookingTableComponent } from './booking-table/booking-table.component';
import { BookingModalComponent } from './booking-modal/booking-modal.component';
import { DeleteBookingModalComponent } from './delete-booking-modal/delete-booking-modal.component';
import { RoomDetailComponent } from './room-detail/room-detail.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import {RoomComponent} from './room/room.component';
import { PricelistComponent } from './pricelist/pricelist.component';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { CrisisListComponent } from './crisis-list/crisis-list.component';
import { StaffListComponent } from './staff-list/staff-list.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {StaffComponent} from './staff/staff.component';
import {StaffDetailComponent} from './staff-detail/staff-detail.component';
import {MessagesComponent} from './messages/messages.component';
import { StaffFormComponent } from './staff-form/staff-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OpenCloseComponent } from './open-close/open-close.component';
import { StatusSliderComponent } from './status-slider/status-slider.component';
import { WorkerListGroupsComponent } from './worker-list-groups/worker-list-groups.component';
import {WorkerListAutoComponent} from './worker-list-auto/worker-list-auto.component';
import {WorkerListAutoPageComponent} from './worker-list-auto-page/worker-list-auto-page.component';
import {WorkerListEnterLeaveComponent} from './worker-list-enter-leave/worker-list-enter-leave.component';
import {WorkerListEnterLeavePageComponent} from './worker-list-enter-leave-page/worker-list-enter-leave-page.component';
import {WorkerListGroupPageComponent} from './worker-list-group-page/worker-list-group-page.component';
import {WorkerListPageComponent} from './worker-list-page/worker-list-page.component';
import { slideInAnimation } from './animations';
import {InsertRemoveComponent} from './insert-remove/insert-remove.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { MenuComponent } from './menu/menu.component';
import { ErrorComponent } from './error/error.component';
import {HttpIntercepterBasicAuthService} from './http-intercepter-basic-auth.service';
import { ConfigComponent } from './config/config.component';
import { DownloaderComponent } from './downloader/downloader.component';
import {HttpErrorHandler} from './http-error-handler-service.service';
import { BookingPersonTableComponent } from './booking-person-table/booking-person-table.component';
import { BookingPersonModalComponent } from './booking-person-modal/booking-person-modal.component';
import { DeleteBookingPersonModalComponent } from './delete-booking-person-modal/delete-booking-person-modal.component';



// import { InMemoryDataService } from './in-memory-data.service';
// import { MessagesComponent } from './messages/messages.component';
// import { RoomSearchComponent } from './room-search/room-search.component';



@NgModule({
  declarations: [
    AppComponent,
    PersonCardComponent,
    PersonComponent,
    WelcomeComponent,
    BookingRoomComponent,
    PersonModalComponent,
    DeletePersonModalComponent,
    BookingTableComponent,
    BookingModalComponent,
    DeleteBookingModalComponent,
    RoomDetailComponent,
    DashboardComponent,
    RoomComponent,
    RoomDetailComponent,
    PricelistComponent,
    CrisisListComponent,
    StaffListComponent,
    StaffComponent,
    StaffDetailComponent,
    PageNotFoundComponent,
    MessagesComponent,
    StaffFormComponent,
    OpenCloseComponent,
    StatusSliderComponent,
    WorkerListAutoComponent,
    WorkerListAutoPageComponent,
    WorkerListEnterLeaveComponent,
    WorkerListEnterLeavePageComponent,
    WorkerListGroupPageComponent,
    WorkerListGroupsComponent,
    WorkerListPageComponent,
    InsertRemoveComponent,
    LoginComponent,
    LogoutComponent,
    MenuComponent,
    ErrorComponent,
    ConfigComponent,
    DownloaderComponent,
    BookingPersonTableComponent,
    BookingPersonModalComponent,
    DeleteBookingPersonModalComponent

    // RoomSearchComponent,
    // MessagesComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      {path: 'person-card', component: PersonCardComponent},
      {path: 'crisis-list', component: CrisisListComponent},
      {path: 'staff-list', component: StaffListComponent},
      {path: '', redirectTo: '/staff-list', pathMatch: 'full'},
      {path: '**', component: PageNotFoundComponent},
      { path: '', pathMatch: 'full', redirectTo: '/enter-leave' },
      { path: 'open-close', component: OpenCloseComponent },
      { path: 'status-slider', component: OpenCloseComponent },
      { path: 'worker-list-page', component: WorkerListPageComponent, data: {animation: 'FilterPage'} },
      { path: 'worker-list-auto', component: WorkerListAutoComponent },
      { path: 'worker-list-enter-leave', component: WorkerListEnterLeaveComponent },
      { path: 'worker-list-enter-leave-page', component: WorkerListEnterLeavePageComponent },
      { path: 'worker-list-group-page', component: WorkerListGroupPageComponent },
      { path: 'worker-list-groups', component: WorkerListGroupsComponent },
      { path: 'insert-remove', component: InsertRemoveComponent }
      ]),
    ReactiveFormsModule,
    NgbDatepickerModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule
],
   // HttpClientInMemoryWebApiModule.forRoot(
      // InMemoryDataService, { dataEncapsulation: false }


  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: HttpIntercepterBasicAuthService, multi: true}
  ],
  bootstrap: [AppComponent]
})

export class AppModule{ }
