import { Component, OnInit } from '@angular/core';
import { Room } from '../room';
import { RoomService } from '../room.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})

export class DashboardComponent implements OnInit {
  rooms: Room[] = [];

  constructor(
    private roomService: RoomService,
    private route: ActivatedRoute,
    private location: Location
    ) { }

  ngOnInit(): void {
  this.roomService.getRooms().subscribe((data: Room[]) => {
  this.rooms = data;
});
}

  getRooms(): void {
    this.roomService.getRooms()
      .subscribe(rooms => this.rooms = rooms.slice(1, 5));
  }
}


