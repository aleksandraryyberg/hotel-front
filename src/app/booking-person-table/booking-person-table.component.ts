import {Component, Input, OnInit} from '@angular/core';
import {BookingService} from '../booking.service';
import {BookingPersonModalComponentService} from '../booking-person-modal/booking-person-modal.component.service';
import {Route, Router} from '@angular/router';
import {Booking} from '../booking';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {Person} from '../person';
import {ActivatedRoute} from '@angular/router';
import { FormBuilder, FormGroup} from '@angular/forms';
import { HttpClient} from '@angular/common/http';
import {PersonService} from '../person.service';
import {DeleteBookingModalComponentService} from '../delete-booking-modal/delete-booking-modal.component.service';
import {RoomService} from '../room.service';
import {BookingPersonService} from '../booking-person.service';
import {BookingPerson} from '../booking-person';
import {DeleteBookingPersonModalComponentService} from '../delete-booking-person-modal/delete-booking-person-modal.component.service';

@Component({
  selector: 'app-booking-person-table',
  templateUrl: './booking-person-table.component.html',
  styleUrls: ['./booking-person-table.component.css']
})
export class BookingPersonTableComponent implements OnInit {
  form: FormGroup;
  bookingPerson: BookingPerson = new BookingPerson();
  bookings: BookingPerson[];
  message: string;
  @Input() bookingPersonRoom: BookingPerson;
  constructor(
    private deleteBookingPersonModalComponentService: DeleteBookingPersonModalComponentService,
    private bookingPersonModalService: BookingPersonModalComponentService,
    private personService: PersonService,
    private roomService: RoomService,
    private bookingService: BookingService,
    private bookingPersonService: BookingPersonService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private http: HttpClient,
  ) { }

  ngOnInit(): void {
    this.bookingPersonService.getBookingPerson().subscribe((data: BookingPerson[]) => {
      this.bookings = data;
    });
  }
  refreshBookingPerson(): void {
    this.bookingPersonService.getBookingPerson().subscribe(
      responce => {
        console.log(responce);
        this.bookings = responce;
        this.ngOnInit();
      }
    );
  }

  deleteThisBookingPerson(id): void {
    this.bookingPersonService.deleteBookingPerson(id).subscribe(
      response => {
        console.log(response);
        this.message = `Delete of booking nr ${id} is Successful!`;
        this.refreshBookingPerson();
        this.ngOnInit();
      }
    );
  }

  updateThisBookingPerson(id): void {
    console.log(`update ${id}`);
    this.router.navigate([`bookingPerson`, id]);
  }

  addBookingPersonDeleteModal(bookingPerson: BookingPerson): NgbModalRef {
    const modalRef = this.deleteBookingPersonModalComponentService.openModal(bookingPerson);
    return modalRef;
  }

  addBookingPersonModal(bookingPerson: BookingPerson): NgbModalRef {
    const modalRef = this.bookingPersonModalService.openModal(bookingPerson);
    return modalRef;
  }
}

