import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingPersonTableComponent } from './booking-person-table.component';

describe('BookingPersonTableComponent', () => {
  let component: BookingPersonTableComponent;
  let fixture: ComponentFixture<BookingPersonTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookingPersonTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingPersonTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
