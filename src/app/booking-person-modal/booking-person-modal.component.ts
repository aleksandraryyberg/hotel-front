import { Component, OnInit } from '@angular/core';
import {Person} from '../person';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BookingPerson} from '../booking-person';
import {Room} from '../room';

@Component({
  selector: 'app-booking-person-modal',
  templateUrl: './booking-person-modal.component.html',
  styleUrls: ['./booking-person-modal.component.css']
})
export class BookingPersonModalComponent implements OnInit {
  bookingPerson: any;
  person: Person;
  room: Room;
  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit(): void {}

  close(): void {
    this.activeModal.dismiss();
  }
}
