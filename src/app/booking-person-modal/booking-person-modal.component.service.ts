import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BookingPersonModalComponent } from './booking-person-modal.component';
import {Booking} from '../booking';
import {BookingPerson} from '../booking-person';


@Injectable({
  providedIn: 'root'
})
export class BookingPersonModalComponentService {
  private isOpen = false;

  constructor(private modalService: NgbModal) {}

  openModal(bookingPerson: BookingPerson): NgbModalRef {
    if (this.isOpen) {
      return;
    }
    this.isOpen = true;
    const modalRef = this.modalService.open(BookingPersonModalComponent, {
      backdrop: 'static',
      size: 'sm'
    });
    modalRef.componentInstance.booking = bookingPerson;
    modalRef.result.then(
      result => {
        this.isOpen = false;
      },
      reason => {
        this.isOpen = false;
      }
    );
    return modalRef;
  }
}
