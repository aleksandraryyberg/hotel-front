import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingPersonModalComponent } from './booking-person-modal.component';

describe('BookingPersonModalComponent', () => {
  let component: BookingPersonModalComponent;
  let fixture: ComponentFixture<BookingPersonModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookingPersonModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingPersonModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
