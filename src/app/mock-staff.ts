import { Worker } from './worker';

export const STAFF: Worker[] = [
  { id: 2, name: 'Hr Nice', lastname: 'Hulaaa', position: 'assistant', age: 24, skill: 'super-strong', alterEgo: 'Maximum'},
  { id: 3, name: 'Alisija', lastname: 'Krag', position: 'housekeeper', age: 65, skill: 'shiny', alterEgo: 'brilliant' },
  { id: 4, name: 'Jaan', lastname: 'Kokk', position: 'cook', age: 55,  skill: 'shiny', alterEgo: 'brilliant'  },
  { id: 5, name: 'Caisy', lastname: 'Kroon', position: 'waiter', age: 37,  skill: 'shiny', alterEgo: 'brilliant' },
  { id: 6, name: 'Sally', lastname: 'Rvoot', position: 'cashier', age: 31,  skill: 'shiny', alterEgo: 'brilliant'  },
];
