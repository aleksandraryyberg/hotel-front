import { Component, OnInit } from '@angular/core';

import { Worker } from '../worker';
@Component({
  selector: 'app-staff-form',
  templateUrl: './staff-form.component.html',
  styleUrls: ['./staff-form.component.css']
})

export class StaffFormComponent {

  skills = ['Really Smart', 'Super Flexible',
    'Super Hot', 'Extra Clean', 'Universal', 'Tragi-tubli'];

  model = new Worker(18, 'Dr New', 'Goblin' , 'Housekeeper', 38, this.skills[0], 'Chuck Overstreet');

  submitted = false;

  onSubmit(): void { this.submitted = true; }

  newWorker(): void {
    this.model = new Worker (42, '', '', '', 22, '', '');
  }
}
