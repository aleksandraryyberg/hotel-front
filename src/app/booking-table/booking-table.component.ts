import {Component, Input, OnInit} from '@angular/core';
import {BookingService} from '../booking.service';
import {BookingModalComponentService} from '../booking-modal/booking-modal.component.service';
import {Route, Router} from '@angular/router';
import {Booking} from '../booking';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {Person} from '../person';
import {ActivatedRoute} from '@angular/router';
import { FormBuilder, FormGroup} from '@angular/forms';
import { HttpClient} from '@angular/common/http';
import {PersonService} from '../person.service';
import {DeleteBookingModalComponentService} from '../delete-booking-modal/delete-booking-modal.component.service';
import {RoomService} from '../room.service';

@Component({
  selector: 'app-booking-table',
  templateUrl: './booking-table.component.html',
  styleUrls: ['./booking-table.component.css']
})
export class BookingTableComponent implements OnInit {
  form: FormGroup;
  booking: Booking = new Booking();
  bookings: Booking[];
  message: string;
  @Input() bookingRoom: Booking;
  constructor(
    private bookingModalService: BookingModalComponentService,
    private deleteBookingModalService: DeleteBookingModalComponentService,
    private personService: PersonService,
    private roomService: RoomService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private bookingService: BookingService
  ) { }

  ngOnInit(): void {
    this.bookingService.getBooking().subscribe((data: Booking[]) => {
      this.bookings = data;
    });
  }
  refreshBooking(): void {
    this.bookingService.getBooking().subscribe(
      responce => {
        console.log(responce);
        this.bookings = responce;
      }
    );
  }

  deleteThisBooking(bookingNr): void {
    this.bookingService.deleteBooking(bookingNr).subscribe(
      response => {
        console.log(response);
        this.message = `Delete of booking nr ${bookingNr} is Successful!`;
        this.refreshBooking();
      }
    );
  }

  updateThisBooking(bookingNr): void {
    console.log(`update ${bookingNr}`);
    this.router.navigate([`bookings`, bookingNr]);
  }

  addBookingDeleteModal(booking: Booking): NgbModalRef {
    const modalRef = this.deleteBookingModalService.openModal(booking);
    return modalRef;
  }

  addBookingModal(booking: Booking): NgbModalRef {
    const modalRef = this.bookingModalService.openModal(booking);
    return modalRef;
  }
  printPersons(persons: Person[]): string {
   // return JSON.stringify(persons);
    let personsText = '';
    for (let i = 0; i < persons.length; i++) {
      personsText += persons[i].name + ' ' + persons[i].surname + ' ' + persons[i].personalCode;
      if (i < persons.length - 1) {
        personsText += ', \n';
      }
    }
    return personsText;
  }
}

