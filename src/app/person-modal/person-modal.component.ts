import {Component, OnInit} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs';
import {Person} from '../person';
import {PersonService} from '../person.service';


@Component({
  selector: 'app-person-modal',
  templateUrl: './person-modal.component.html',
  styleUrls: ['./person-modal.component.css']
})

export class PersonModalComponent implements OnInit {
  person: any;
  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit(): void {}

  close(): void {
    this.activeModal.dismiss();
  }
}

