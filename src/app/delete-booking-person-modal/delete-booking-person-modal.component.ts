import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs';
import {Booking} from '../booking';
import {Route, Router} from '@angular/router';
import {OperationResultBooking} from '../operation-result-booking';
import { BookingPersonModalComponentService } from '../booking-person-modal/booking-person-modal.component.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {BookingService} from '../booking.service';
import {FormGroup} from '@angular/forms';
import {DeleteBookingPersonModalComponentService} from './delete-booking-person-modal.component.service';
import {BookingPerson} from '../booking-person';
import {BookingPersonService} from '../booking-person.service';

@Component({
  selector: 'app-delete-booking-person-modal',
  templateUrl: './delete-booking-person-modal.component.html',
  styleUrls: ['./delete-booking-person-modal.component.css']
})
export class DeleteBookingPersonModalComponent implements OnInit {
  message: string;
  form: FormGroup;
  submitted = false;
  bookingPerson: BookingPerson = new BookingPerson();
  posts: any;
  bookings: BookingPerson[];
  constructor(
    private activeModal: NgbActiveModal,
    private bookingService: BookingService,
    private bookingPersonService: BookingPersonService,
    private router: Router,
    private deleteBookingPersonModalComponentService: DeleteBookingPersonModalComponentService
  ) { }

  ngOnInit(): void { }

  DeleteThisBookingPerson(): void {
    this.submitted = true;
// stop here if form is invalid
    this.bookingPersonService.deleteBookingPerson(this.bookingPerson.id).subscribe(
      response => {
        console.log(response);
        this.message = `Delete of booking nr ${this.bookingPerson.id} is Successful!`;
        this.bookingPersonService.getBookingPerson().subscribe();
      }
    );
    this.bookingPersonService.getBookingPerson().subscribe(
      responce => {
        console.log(responce);
        this.bookings = responce;
      }
    );
  }
  isError(): boolean{
    return false; // this.registerForm.invalid;
  }
  close(): void {
    this.activeModal.dismiss();
  }
}


