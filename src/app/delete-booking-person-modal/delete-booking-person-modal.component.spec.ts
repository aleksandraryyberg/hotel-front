import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteBookingPersonModalComponent } from './delete-booking-person-modal.component';

describe('DeleteBookingPersonModalComponent', () => {
  let component: DeleteBookingPersonModalComponent;
  let fixture: ComponentFixture<DeleteBookingPersonModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteBookingPersonModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteBookingPersonModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
