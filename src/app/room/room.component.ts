import { Component, OnInit } from '@angular/core';
import {RoomService} from '../room.service';
import {Room} from '../room';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {PersonService} from '../person.service';
import {Person} from '../person';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})

export class RoomComponent implements OnInit {
  constructor(private roomService: RoomService) { }

  rooms: Room[];
  checks = false;

  ngOnInit(): void {
    this.roomService.getRooms().subscribe((data: Room[]) => {
      this.rooms = data;
    });
  }
  bulk(e): void {
    if (e.target.checked === true) {
      this.checks = true;
    } else {
      this.checks = false;
    }
  }
}


