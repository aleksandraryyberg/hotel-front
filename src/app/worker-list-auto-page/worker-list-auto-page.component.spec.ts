import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerListAutoPageComponent } from './worker-list-auto-page.component';

describe('WorkerListAutoPageComponent', () => {
  let component: WorkerListAutoPageComponent;
  let fixture: ComponentFixture<WorkerListAutoPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkerListAutoPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerListAutoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
