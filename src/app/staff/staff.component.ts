import { Component, OnInit } from '@angular/core';
import {Worker} from '../worker';
import { STAFF } from '../mock-staff';
import {StaffService} from '../staff.service';
import {MessageService} from '../message.service';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})

export class StaffComponent implements OnInit {
  // staff = STAFF;
  // selectedWorker: Worker;
  staff: Worker[];
  constructor(private staffService: StaffService) { }
              // private messageService: MessageService
  ngOnInit(): void {
    this.getStaff();
  }

  // onSelect(worker: Worker): void {
   //  this.selectedWorker = worker;
   //  this.messageService.add(`StaffComponent: Selected worker id=${worker.id}`);
 // }

  getStaff(): void {
    this.staffService.getStaff()
      .subscribe(staff => this.staff = staff);
  }
}


