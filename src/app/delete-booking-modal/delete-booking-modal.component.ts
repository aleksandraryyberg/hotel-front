import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs';
import {Booking} from '../booking';
import {Route, Router} from '@angular/router';
import {OperationResultBooking} from '../operation-result-booking';
import { BookingModalComponentService } from '../booking-modal/booking-modal.component.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {BookingService} from '../booking.service';
import {FormGroup} from '@angular/forms';
import {DeleteBookingModalComponentService} from './delete-booking-modal.component.service';

@Component({
  selector: 'app-delete-booking-modal',
  templateUrl: './delete-booking-modal.component.html',
  styleUrls: ['./delete-booking-modal.component.css']
})
export class DeleteBookingModalComponent implements OnInit {
  message: string;
  form: FormGroup;
  submitted = false;
  booking: Booking = new Booking();
  posts: any;
  bookings: Booking[];
constructor(
    private activeModal: NgbActiveModal,
    private bookingService: BookingService,
    private router: Router,
    private deleteBookingModalComponentService: DeleteBookingModalComponentService
  ) { }

  ngOnInit(): void { }

  DeleteThisBooking(): void {
    this.submitted = true;
// stop here if form is invalid
    this.bookingService.deleteBooking(this.booking.bookingNr).subscribe(
      response => {
        console.log(response);
        this.message = `Delete of person nr ${this.booking.bookingNr} is Successful!`;
        this.bookingService.getBooking().subscribe();
      }
    );
    this.bookingService.getBooking().subscribe(
      responce => {
        console.log(responce);
        this.bookings = responce;
      }
    );
  }
  isError(): boolean{
    return false; // this.registerForm.invalid;
  }
  close(): void {
    this.activeModal.dismiss();
  }
}

