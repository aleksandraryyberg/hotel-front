import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteBookingModalComponent } from './delete-booking-modal.component';

describe('DeleteBookingModalComponent', () => {
  let component: DeleteBookingModalComponent;
  let fixture: ComponentFixture<DeleteBookingModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteBookingModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteBookingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
