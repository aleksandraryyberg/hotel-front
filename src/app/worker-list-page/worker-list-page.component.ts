import { Component, HostBinding, OnInit } from '@angular/core';
import { trigger, transition, animate, style, query, stagger } from '@angular/animations';
import { STAFF } from '../mock-staff';

@Component({
  selector: 'app-worker-list-page',
  templateUrl: 'worker-list-page.component.html',
  styleUrls: ['worker-list-page.component.css'],
  animations: [
    trigger('pageAnimations', [
      transition(':enter', [
        query('.worker, form', [
          style({opacity: 0, transform: 'translateY(-100px)'}),
          stagger(-30, [
            animate('500ms cubic-bezier(0.35, 0, 0.25, 1)', style({ opacity: 1, transform: 'none' }))
          ])
        ])
      ])
    ]),
    trigger('filterAnimation', [
      transition(':enter, * => 0, * => -1', []),
      transition(':increment', [
        query(':enter', [
          style({ opacity: 0, width: '0px' }),
          stagger(50, [
            animate('300ms ease-out', style({ opacity: 1, width: '*' })),
          ]),
        ], { optional: true })
      ]),
      transition(':decrement', [
        query(':leave', [
          stagger(50, [
            animate('300ms ease-out', style({ opacity: 0, width: '0px' })),
          ]),
        ])
      ]),
    ]),
  ]
})
export class WorkerListPageComponent implements OnInit {
  @HostBinding('@pageAnimations')
  public animatePage = true;

  workerTotal = -1;
  get staff() { return this._staff; }
  // tslint:disable-next-line:variable-name
  private _staff = [];

  ngOnInit(): void {
    this._staff = STAFF;
  }

  updateCriteria(criteria: string): void {
    criteria = criteria ? criteria.trim() : '';

    this._staff = STAFF.filter(worker => worker.name.toLowerCase().includes(criteria.toLowerCase()));
    const newTotal = this.staff.length;

    if (this.workerTotal !== newTotal) {
      this.workerTotal = newTotal;
    } else if (!criteria) {
      this.workerTotal = -1;
    }
  }
}
