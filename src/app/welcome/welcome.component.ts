import { WelcomeDataService} from '../welcome-data.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})


export class WelcomeComponent implements OnInit {

  message = 'Some Welcome Message'
  welcomeMessageFromService: string;
  name = '';
  constructor(
    private route: ActivatedRoute,
    private service: WelcomeDataService) {

  }

  // void init() {
  ngOnInit(): void {
    this.name = this.route.snapshot.params.name;

  }

  getWelcomeMessage(): void {
    console.log(this.service.executeHelloWorldBeanService());

    this.service.executeHelloWorldBeanService().subscribe(
      response => this.handleSuccessfulResponse(response),
      error => this.handleErrorResponse(error)
    );
    console.log('last line of getWelcomeMessage');

    console.log('get welcome message');
  }

  getWelcomeMessageWithParameter(): void {
    console.log(this.service.executeHelloWorldBeanService());

    this.service.executeHelloWorldServiceWithPathVariable(this.name).subscribe(
      response => this.handleSuccessfulResponse(response),
      error => this.handleErrorResponse(error)
    );
  }


  handleSuccessfulResponse(response): void {
    this.welcomeMessageFromService = response.message;

  }

  handleErrorResponse(error): void {
    this.welcomeMessageFromService = error.error.message;
  }
}

export class Class1 {

}

export class Class2 {

}
