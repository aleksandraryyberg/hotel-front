import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerListGroupsComponent } from './worker-list-groups.component';

describe('WorkerListGroupsComponent', () => {
  let component: WorkerListGroupsComponent;
  let fixture: ComponentFixture<WorkerListGroupsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkerListGroupsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkerListGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
