import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Worker } from './worker';
import { STAFF } from './mock-staff';
import { MessageService } from './message.service';


@Injectable({
  providedIn: 'root',
})
export class StaffService {

  constructor(private messageService: MessageService) { }

  getStaff(): Observable<Worker[]> {
    // TODO: send the message _after_ fetching the heroes
    this.messageService.add('WorkerService: fetched staff');
    return of(STAFF);
  }
  getWorker(id: number): Observable<Worker> {
    // TODO: send the message _after_ fetching the hero
    this.messageService.add(`StaffService: fetched worker id=${id}`);
    return of(STAFF.find(worker => worker.id === id));
  }
}
